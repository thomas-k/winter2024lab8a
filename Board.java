public class Board{
	private Tile[][] grid;
	
	//constructor
	public Board(){
		this.grid = new Tile[3][3];
		
		for(int i = 0; i < this.grid.length; i++){
			for(int j = 0; j < this.grid[i].length; j++){
				this.grid[i][j] = Tile.BLANK;
			}
		}
	}
	
	// to String
	public String toString(){
		String output = "";
		String filler = "+---+---+---+";
		
		for(Tile[] t1: this.grid){
			output += "\n" + filler + "\n| ";
			for(Tile t2: t1){
				output += t2.getName() + " | ";
			}
		}
		output += "\n" + filler;
		return output;
	}
	
	public boolean placeToken(int row, int col, Tile playerToken){
		if(row > 3 || col > 3) return false;
		if(this.grid[row - 1 ][col - 1] == Tile.BLANK){
			this.grid[row - 1][col - 1] = playerToken;
			return true;
		} else return false;
	}
	
	public boolean checkFull(){
		for(Tile[] row: this.grid){
			for(Tile t: row){
				if(t == Tile.BLANK) return false;
			}
		}
		return true;
	}
	
	private boolean checkIfWinningHorizontal(Tile playerToken){
		for(Tile[] row: this.grid){
			boolean rowFull = true;
			for(Tile t: row){
				rowFull = rowFull && (t == playerToken);
			}
			if(rowFull) return true;
		}
		return false;
	}
	
	private boolean checkIfWinningVertical(Tile playerToken){
		boolean col1 = true;
		boolean col2 = true;
		boolean col3 = true;
		for(Tile[] row: this.grid){
			for(int i = 0; i < row.length; i++){
				if(i == 0) col1 = col1 && row[i] == playerToken;
				if(i == 1) col2 = col2 && row[i] == playerToken;
				if(i == 2) col3 = col3 && row[i] == playerToken;
			}
		}
		return col1 || col2 || col3;
	}
	
	private boolean checkIfWinningDiagonal(Tile playerToken){
		boolean diagLeft = true;
		boolean diagRight = true;
		for(int i = 0; i < this.grid.length; i++){
			diagLeft = (this.grid[i][i] == playerToken) && diagLeft;
			for(int j = 0; j < this.grid[i].length; j++){
				if((i == 0 && j == 2) || (i == 1 && j == 1) || (i == 2 && j == 0)){
					diagRight = (this.grid[i][j] == playerToken) && diagRight;
				}
			}
		}
		return diagLeft || diagRight;
		
	}
	
	public boolean checkIfWinning(Tile playerToken){
		boolean winH = checkIfWinningHorizontal(playerToken);
		boolean winV = checkIfWinningVertical(playerToken);
		boolean winD = checkIfWinningDiagonal(playerToken);
		return winH || winV || winD;
	}
	
	public Tile[][] getGrid(){
		return this.grid;
	}
}