import java.util.Scanner;

public class TicTacToeApp{
	public static void main(String[] args){
		Scanner reader = new Scanner(System.in);
		System.out.println("Welcome to this game of Tic-Tac-Toe!");
		System.out.println("Player 1 will be playing with the token X, player 2 will use O");
		
		Board b1 = new Board();
		
		boolean gameOver = false;
		int player = 1;
		Tile playerToken = Tile.X;
		
		while(!gameOver){
			//getting player input
			System.out.println("\nxoxoxoxoxoxoxoxoxoxoxoxoxoxoxo\n\nPlayer " + player + ", your turn!");
			System.out.println(b1 + "\nPlease type the row (1-3) of the cell you choose to place your token in");
			int row = Integer.parseInt(reader.nextLine());
			System.out.println("\nPlease type the column (1-3) of the cell you choose to place your token in");
			int col = Integer.parseInt(reader.nextLine());
			
			if(player == 1) playerToken = Tile.X;
			else playerToken = Tile.O;
			
			//if player input is invalid
			while(!b1.placeToken(row, col, playerToken)){
				System.out.println("\nInvalid placement. Try again.");
				System.out.println("\nPlease type the row (1-3) of the cell you choose to place your token in");
				row = Integer.parseInt(reader.nextLine());
				System.out.println("Please type the column (1-3) of the cell you choose to place your token in");
				col = Integer.parseInt(reader.nextLine());
			}
			
			System.out.println(b1);
			
			//winning conditions
			if(b1.checkIfWinning(playerToken)){
				System.out.println("Player " + player + " is the winner!");
				gameOver = true;
			} else if(b1.checkFull()){
				System.out.println("It's a tie!");
				gameOver = true;
			} else {
				player++;
				if(player > 2) player = 1;
			}
			
		}
	}
}