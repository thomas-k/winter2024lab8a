enum Tile{
	BLANK("_"),
	X("X"),
	O("O");
	
	private Tile(final String name){
		this.name = name;
	}
	
	private String name;
	
	public String getName(){
		return this.name;
	}
}